class PagesController < ApplicationController
  before_action :authenticate_user!

  def home

  end

  def register
    @user = User.new(user_params)

   if @user.save
     render json: @user, status: :created, location: @user
   else
     render json: @user.errors, status: :unprocessable_entity
   end
  end




  private
   # Use callbacks to share common setup or constraints between actions.
   def set_user
     @user = User.find(params[:id])
   end

   # Only allow a trusted parameter "white list" through.
   def user_params
     params.permit(:name, :cpf, :password_confirmation,:password, :email, :telephone,:sex,:birth_date)
   end


end
