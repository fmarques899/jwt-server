Rails.application.routes.draw do
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  get 'pages/home'

  devise_for :users
  post 'register' , to: 'pages#register'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root to: "home#index"
  mount JwtDevise::Engine, at: '/jwt'
end
